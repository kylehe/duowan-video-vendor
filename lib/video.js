var fetch = require('node-fetch');
var cheerio = require('cheerio');
var Cache = require('cache');


const UA = 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1'

var cache = new Cache(3600 * 1000)


function getUrl(vid) {
    return `http://video.duowan.cn/play/${vid}.html`
}

function getSrc(vid) {
    var src = cache.get(vid)
    if (src) {
        return Promise.resolve(src)
    }

    return fetch(getUrl(vid), {
        headers: {
            'User-Agent': UA
        }
    }).then(function (res) {
        return res.text()
    }).then(function (content) {
        var $ = cheerio.load(content)
        var src = $('#video').attr('src')
        cache.put(vid, src)
        return src
    }).catch(function (err) {
        return ''
    })
}

exports.getSrc = getSrc
