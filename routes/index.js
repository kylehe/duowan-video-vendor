var express = require('express');
var router = express.Router();
var { getSrc } = require('../lib/video')


/* GET home page. */
router.post('/video', function(req, res, next) {
    var {vid, key} = req.body;

    if (key != '6fFyvNrx4MQaJniAOi' || !/^\d+$/.test(vid)) {
        res.send({}).end();
        return;
    }

    getSrc(vid).then(function (src) {
        res.send({src, vid}).end()
    })

});

module.exports = router;
